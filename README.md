# graphclj

DM2 made by Rettana Muon.  
All function seems to work properly.  

###Issues :
Nodes keys are "\k" instead of "k".   
Solution : Transformed value from k to \k (\k = (char (int k)) + 48)  
Issue : Can't handle more than 10 nodes. User graph definition have to be {\k0 {:neigh #{\k1 \k2 ...} ...} ...}  

### Main function :
Run command with : lein run [args]  
-g graph : Set a particular graph   
-gen-rnd nb_nodes prob_pair : generate a random graph with a certain probability of pairing (up to 10 nodes)  
-deg : add degrees information   
-close node_val : return the value of the node  
-close-all : add closeness information  
-dot : change the graph into a .dot format  
-rank label : sort the graph per label (:close or :degree)  
  
Main notes :   
Multiple arguments can be cast together.  
Only -close and -dot can't be used together (:dot will be used when both are casted)  

subnotes :  
There is no research for bad typed or inexistant arguments.  
There is no check of arguments (ie : -gen-rnd 5 a will take "a" as second argument)   
