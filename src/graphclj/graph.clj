(ns graphclj.graph
  (:require [clojure.string :as str]))

;; Generate a graph from the lines
(defn gen-graph [lines]
  ;; Transfrom the string list into a list of graph data reprenstation
  ;; from "node1 node2" to {node1 {:neigh #{node2}}
  ;; _n1 contains node1 -> node2 representations
  ;; _n2 contains node1 <- node2 representations
  (def _n1 (vec (for [l lines] {(get l 0) {:neigh #{(get l 2)}}})))
  (def _n2 (vec (for [l lines] {(get l 2) {:neigh #{(get l 0)}}})))
  ;; Merge _n1 and _n2 into n
  (def n (reduce into [_n1 _n2] ))
  ;; Merge the duplicates keys and adds their :neigh value into the set
  (reduce
   (fn [res n]
     (do
       (def k (get (vec (keys n)) 0 )) ;; key of the node
       (def v  (get(vec (vals n)) 0 )) ;; value of the node : {:neigh #{v}}
       (def res_v (get res k)) ;; return nil if res doesn't have a value yet.
       (if (= nil res_v)
         ;; res_v == nil : add a new value
         (assoc res k v)
         ;; res_v != nil : merge the values from the already existing values and the new one
         (do
           (def new_v (merge-with into res_v v )) ;; merged value
           (assoc res k new_v)
           )
         )
       )
     )
   {} ;; res
   n  ;; data list
   )
  )

;; Generate random graph
(defn erdos-renyi-rnd [n,p]
  ;; Make random graph. Consider a node can't link itself and graph is undirected.
  ;; Browse the tab like in this example (considering n = 5).
  ;; \ 0 1 2 3 4        x = random not tested for this combination
  ;; 0|x x x x x        o = random tested for this combination
  ;; 1|o x x x x
  ;; 2|o o x x x
  ;; 3|o o o x x
  ;; 4|o o o o x
  (def res (vec (for [i  (range n)]
                  (vec(for [j  (range (- n (- n i)))]
                        (if (= i j)
                          nil
                          (if (< (rand) p)
                            (str i " " j)
                            )
                          )
                        ))
                  )))
  ;; merge the vec from both loops and remove nils
  (remove nil? (reduce into res))
  )
