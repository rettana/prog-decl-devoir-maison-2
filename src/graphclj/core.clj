(ns graphclj.core
  (:require [graphclj.graph :as graph]
            [graphclj.tools :as tools]
            [graphclj.centrality :as central])
  (:gen-class))

(defn -main
  [& args]
  (def argsVec (vec args) )
  (if (= args nil)
    (do
      (println "0 argument... Here's a random graph with 5 nodes and a probability of 0.5 for each node to have a pair")
      (println (graph/gen-graph(graph/erdos-renyi-rnd 5 0.5)))
      )
    (def argMap (into {} (remove nil?(vec(for [n (range (count argsVec))]
               (do
                 (def arg (get argsVec n))
                 (if (= arg "-gen-rnd")
                   (do
                     (def a1 (get argsVec (+ n 1)))
                     (def a2 (get argsVec (+ n 2)))
                     {:gen-rnd [a1 a2]}
                     )
                   (if (= arg "-g")
                     (do
                       (def a1 (get argsVec (+ n 1)))
                       {:g a1}
                       )
                     (if (= arg "-deg")
                       {:deg 0}
                       (if (= arg "-close")
                         (do
                           (def a1 (get argsVec (+ n 1)))
                           {:close a1}
                           )
                         (if (= arg "-close-all")
                           {:close-all 0}
                           (if (= arg "-rank")
                             (do
                               (def a1 (get argsVec (+ n 1)))
                               {:rank a1}
                               )
                             (if (= arg "-dot")
                               {:dot 0}
                               )
                             )
                           )
                         )
                       )
                     )
                   )
                 )
               )))))

  )
  (println argMap)
  (if (contains? argMap :g)
    (def graph a1)
    (if (contains? argMap :gen-rnd)
      (do
        (def v (get argMap :gen-rnd))
        (def a1 (read-string (get v 0)))
        (def a2 (read-string (get v 1)))
        (def graph (graph/gen-graph (graph/erdos-renyi-rnd a1 a2)))
        )
      (do
        (println "No argument for the graph... a random graph with 5 nodes and a probability of 0.5 for each node to have a pair will be made instead")
        (def graph (graph/gen-graph (graph/erdos-renyi-rnd 5 0.5)))
        )
      )
    )
  (if (contains? argMap :deg)
    (def graph (central/degrees graph))
    )
  (if (contains? argMap :close-all)
    (def graph (central/closeness-all graph))
    )
  (if (contains? argMap :rank)
    (do
      (def a1 (read-string(get v 0)))
      (def graph (tools/rank-nodes graph a1))
      )
    )
  (def res graph)
  (if (contains? argMap :close)
    (def res (central/closeness graph (read-string(get argMap :close))))
    (if (contains? argMap :dot)
      (def res (tools/to-dot (central/closeness-all graph)))
      )
    )
  (println "Graph = " graph)
  (println res)
  )
