(ns graphclj.centrality
    (:require [graphclj.graph :as graph]
              [clojure.set :as set]))


;;  Calculates the degree centrality for each node
(defn degrees [g]
  (reduce
   (fn [res g]
     (def k (get g 0 )) ;; key of the node
     (def v (get g 1 )) ;; val of the node
     (def new_v (assoc v :degree (count (get v :neigh)))) ;; add degree
     (assoc res k new_v) ;; merge values into res
     )
   {} ;; res
   g
   )
  )
(defn distance [g n]
  ;; Problem statement uses representation of graph : { 0 { :neigh #{v} }, 1 { :neigh #{v} }
  ;; Keys seems to be '0','1','2',[...] but in reality it is '\0', '\1', '\2', [...]
  ;; Number of node limited to 10 because of use of \0 to \9

  ;; ## [Some functions]
  ;; Return char value from n to \n
  ;; Value of node from 'n' to '\n' is (int 48) further
  (defn getArgValue [n]
    (char (+ 48 (int n)))
    )
  ;; Return :neigh of a node n considering n as an user input.
  (defn getNeighFromArg [g n]
    (get  (get g (getArgValue n)) :neigh)
    )

  ;; return :neigh of a node n
  (defn getNeigh [g n]
    (get (get g n ) :neigh)
    )

  ;; Removes all the nodes which already exists in res.
  ;; neigh : actual set of browsed graph
  ;; res : result
  (defn removeTraveledNodes [neigh res]
    (set(remove nil? (for [n neigh]
                       (if (contains? res n)
                         nil
                         n
                         )
                       )))
    )

  ;; Fetch all neighboor nodes from the actual neigh. Removes all nodes already reached once.
  ;; g : graph
  ;; neigh : actual set of browsed graph
  ;; res : result
  (defn getNextNeigh [g neigh res]
    (removeTraveledNodes
     (reduce into (set(for [n neigh]
                        (getNeigh g n )
                        )))
     res
     )
    )

  ;; recursive read of the graph
  ;; g : graph
  ;; neigh : actual set of browsed graph
  ;; d : actual distance
  ;; res : result
  ;; n0 : start node key
  (defn distance_recur [g neigh d res n0]
    ;; Getting the new res : adding each new nodes reached to res with the distance value
    (def newres (reduce (fn [res n] (assoc res n d)) res neigh))
    (if (= neigh #{} )
      (dissoc newres n0) ;; removes the start node
      (distance_recur
       g
       (getNextNeigh g neigh newres)
       (+ d 1) ;; inc distance
       newres
       n0
       )
      )
    )
  ;; [End of functions]
  ;; ## Main code of (distance [g n])
  ;; start node value
  (def neigh0 (getNeighFromArg g n))
  (distance_recur g neigh0 1 {} (getArgValue n) )
  )

;;  Returns the closeness for node n in graph g
;;  g : graph
;;  n : a node
(defn closeness [g n]
  (def distance_map (distance g n) )
  (float (reduce
          (fn [res d] ;; d is a vec [node, distance]
            (+ res (/ 1 (get d 1)))
            )
          0
          distance_map
          ))
  )


;; Returns the closeness for all nodes in graph g
;; g : a graph
(defn closeness-all [g]
  ;; Distance is converting value from n to \n.
  ;; This function will convert value from \n to n
  (defn reverseGetArgValue [n]
    (char (- (int n) 48))
    )

  ;; main function
  (reduce
   (fn [res node]  ;; node is a vec [key val]
     (def k (get node 0))
     (def v (get node 1))
     (assoc res k (assoc v :close (closeness g (reverseGetArgValue k) )))
     )
   g
   g
   )
  )


