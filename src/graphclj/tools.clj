(ns graphclj.tools
  (:require [clojure.string :as str])
  (:require [graphclj.centrality :as central]))

(defn readfile [f]
  "Returns a sequence from a file f"
  (with-open [rdr (clojure.java.io/reader f)]
    (doall (line-seq rdr))))

;; Ranks the nodes of the graph in relation to label l in accending order
;; g : a graph
;; l : a label (:close or :degree)
(defn rank-nodes [g,l]
  (into {} (sort
            (fn [x y]
              (compare (get (get x 1) l)  (get (get y 1) l) )
              )
            ;; In the example, the result had :degree and :close.
            ;; However closeness do not have :degree included in his example too and I did not implemented that.
            ;; Thus I am casting degrees here.
            (central/degrees g)
            ))
  )

(defn generate-colors [n]
  (let [step 10]
    (loop [colors {}, current [255.0 160.0 122.0], c 0]
      (if (= c (inc n))
        colors
        (recur (assoc colors c (map #(/ (mod (+ step %) 255) 255) current))
          (map #(mod (+ step %) 255) current) (inc c))
        ))))


;; Returns a string in dot format for graph g, each node is colored in relation to its ranking
(defn to-dot [g]
  ;;Generating colors...
  (def colors (generate-colors(- (count g) 1)))
  (def intro "graph g {\n")
  ;; Step 1 : Generate color part
  (def colStr (reduce
               (fn [res n]
                 (def k (get n 0)) ;; n is a vec [key value] from the graph g which is a map
                 (def v (vec(get n 1)))
                 (def vToString  (str " [style=filled color=" \" (get v 0)  " " (get v 1) " " (get v 2) \" "]\n"))
                 (str res k " " vToString)
                 )
               ""
               colors
               )
    )
  ;; Step 2 : Generate graph links
  (def linksStr (reduce
                 (fn [res n]
                   (def k (get n 0)) ;; n is a vec [key value] from the graph g which is a map
                   (def v (get n 1))
                   (def neigh (get v :neigh))
                   (str res (apply str
                                   (for [n neigh]
                                     (if (< (int n) (int k))
                                       ""
                                       (str k " -- " n "\n")
                                       )
                                     )
                                   ))
                   )
                 ""
                 (sort g)
                 ))
  (def outro "}")
  (str intro colStr linksStr outro)
  ;; Result has nil at the end but I don't know why.
  ;; result :
  ;; graph g{
  ;;  [vals]
  ;; }
  ;; nil

  ;; Even with an arbitrary result it has a nil at the end:
  )
